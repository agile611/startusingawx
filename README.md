[![Agile611](https://www.agile611.com/wp-content/uploads/2020/09/cropped-logo-header.png)](http://www.agile611.com/)

# Agile611 AWX Demo 

This repository contains the installation from AWX from . It uses Vagrant.

## Requirements

This repo uses a Vagrant box based on Ubuntu.

## Example code

Clone this repository with:

```shell
git clone https://bitbucket.org/agile611/startusingawx.git
```

## Initial configuration

* Start environment, we are going to need 4 ubuntu boxes (Ansible, Alfa, Bravo, Charlie)

```shell
vagrant up 
vagrant ssh awx
```


## Support

This tutorial is released into the public domain by Agile611 under WTFPL.

[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)](http://www.wtfpl.net/)

This README file was originally written by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhs/) and is likewise released into the public domain.

Please contact Agile611 for further details.

* Agile611
* Laureà Miró 309
* 08950 Esplugues de Llobregat (Barcelona)