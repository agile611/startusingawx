#!/bin/bash
set -eux
apt-get update
apt-get install -y --no-install-recommends \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common \
        python3-minimal zip python3-simplejson \
        gnupg2 python-pip-whl python3-pip\
        software-properties-common
apt-get remove docker docker.io -y

curl -fsSL get.docker.com -o get-docker.sh
sh get-docker.sh
# install docker-compose
curl -L "https://github.com/docker/compose/releases/download/v2.0.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && \
    chmod +x /usr/local/bin/docker-compose

# install docker-machine
curl -L "https://github.com/docker/machine/releases/download/v0.16.2/docker-machine-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-machine && \
    chmod +x /usr/local/bin/docker-machine

usermod -G docker vagrant
